Building
========

docker build -t girish/gitblit .

Running
=======
docker run -tiP -h domain.com \
    -e MAIL_DOMAIN=foo.com \
    -e MAIL_SMTP_SERVER=loopback \
    -e MAIL_SMTP_USERNAME=gitblit \
    -e MAIL_SMTP_PORT=25 \
    -v /tmp/gitblitdata:/app/data girish/gitblit

