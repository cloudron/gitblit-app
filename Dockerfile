FROM girish/base:0.1.0
MAINTAINER girish@forwardbias.in

RUN mkdir -p /app/code
WORKDIR /app/code
RUN curl -L http://dl.bintray.com/gitblit/releases/gitblit-1.6.2.tar.gz | tar -xz -f -

ADD start.sh /app/code/start.sh
RUN chmod +x /app/code/start.sh

# http
EXPOSE 8080

# git-daemon
EXPOSE 9418

# ssh
EXPOSE 29418

CMD [ "/app/code/start.sh" ]

