#!/bin/bash

set -eux

if [[ -z "$(ls -A /app/data)" ]]; then
    cp -r /app/code/data/* /app/data/

    curl -L http://lorempixel.com/100/100/ > /app/data/logo.png
fi

: ${MAIL_SMTP_SERVER:-}
: ${MAIL_SMTP_PORT:-}
: ${MAIL_SMTP_USERNAME:-}
: ${MAIL_DOMAIN:-}

sed -e "s/^web.canonicalUrl =.*/web.canonicalUrl = https:\/\/$(hostname -f)/" \
    -e "s/^mail.server =.*/mail.server = ${MAIL_SMTP_SERVER}/" \
    -e "s/^mail.port =.*/mail.port = ${MAIL_SMTP_PORT}/" \
    -e "s/^mail.fromAddress =.*/mail.fromAddress = ${MAIL_SMTP_USERNAME}@${MAIL_DOMAIN}/" \
    -e "s/^server.httpPort =.*/server.httpPort = 8080/" \
    -e "s/^server.httpsPort =.*/server.httpsPort = 0/" \
    -e "s/^server.redirectToHttpsPort =.*/server.redirectToHttpsPort = true/" \
    -e "s/^server.tempFolder =.*/server.tempFolder = \/tmp\/gitblit/" \
    -i /app/data/gitblit.properties

java -jar /app/code/gitblit.jar --baseFolder /app/data

